exports.up = function (knex, Promise) {
  return knex.schema.createTable('food', table => {
    table.increments('id').primary()
    table.string('name')
    table.string('description')
    table.float('weight')
    const nutrients = ['proteins', 'carbohydrates', 'lipids']
    nutrients.forEach(nut => table.float(nut))
  })
}

exports.down = function (knex, Promise) {
  return knex.schema.dropTable('food')
}
